package com.example.shankar.recyclerviewtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<Movie> movieList=new ArrayList<Movie>();
    private RecyclerView mRecyclerView;
    private MainAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView=(RecyclerView)findViewById(R.id.recycler);
        //mRecyclerView.setHasFixedSize(true);
       mAdapter=new MainAdapter(movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
        prepareMovieData();
    }

    private void prepareMovieData() {
        Movie movie = new Movie("Mad Max: Fury Road", 2015);
        movieList.add(movie);

        movie = new Movie("Inside Out", 2015);
        movieList.add(movie);

        movie = new Movie("Star Wars: Episode VII - The Force Awakens", 2015);
        movieList.add(movie);

        movie = new Movie("Shaun the Sheep", 2015);
        movieList.add(movie);

        movie = new Movie("The Martian", 2015);
        movieList.add(movie);

        movie = new Movie("Mission: Impossible Rogue Nation", 2015);
        movieList.add(movie);

        movie = new Movie("Up",2009);
        movieList.add(movie);

        movie = new Movie("Star Trek",2009);
        movieList.add(movie);

        movie = new Movie("The LEGO Movie",2014);
        movieList.add(movie);

        movie = new Movie("Iron Man",2008);
        movieList.add(movie);

        movie = new Movie("Aliens", 1986);
        movieList.add(movie);

        movie = new Movie("Chicken Run",2000);
        movieList.add(movie);

        movie = new Movie("Back to the Future",1985);
        movieList.add(movie);

        movie = new Movie("Raiders of the Lost Ark",1981);
        movieList.add(movie);

        movie = new Movie("Goldfinger",1965);
        movieList.add(movie);

        movie = new Movie("Guardians of the Galaxy",2014);
        movieList.add(movie);

        mAdapter.notifyDataSetChanged();
    }
}
